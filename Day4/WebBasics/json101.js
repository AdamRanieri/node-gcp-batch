
// JSO Object Literal
// a data strcutre in JS
const adam = {
    fname:"Adam", 
    lname:"Ranieri", 
    age:19, 
    isTrainer:true,
    desecribeSelf: function(){
        console.log(`My name is ${this.fname}`)
    }
};

const adamJSON = JSON.stringify(adam); // A JSON IS A STRING!!!!
console.log(adamJSON)

const player = {position:"Forward", points:34};
// {"position":"Forward", "points":34}

const ray = [{name:"Adam"}, {name:"steve"}];
const rayJSON = JSON.stringify(ray);
console.log(rayJSON);

const dog = JSON.parse('{"name":"Rover","owner":"tim"}')
console.log(dog)