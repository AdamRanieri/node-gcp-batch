# RevAssess
## Purpose
Revature wants to give software development challenges. Smaller coding assignments can be given via numerous websites but Revature wants more advanced and holistic challenges. Examples, could include, creating a DAO, debugging an application, or implementing RESTful endpoints. Trainers want to use the results of the challenges to assess the batch as a whole and to view individual code to create targeted feedback. There is a working proof of concept prototype https://revassess.firebaseapp.com/analytics. There is also documetnation going through the RevAssess process. https://github.com/adamranieri/RevAssessTutorial/blob/master/RevAssessSteps.md

## How It works
### Creating an Exercise
1. A Trainer creates a code skeleton and uploads it to GitLab or GitHub.
2. A Trainer logs into RevAssess
3. A Trainer creates an **Exercise** on the RevAssess Website
    - The Trainer should put in the github/gitlab link
    - The Trainer should put in the name of the Exercise (Must be Unique)
### Taking A RevAssess
1. An Associate uses git to clone the repository
2. An Associate opens up the cloned repository in their IDE of choice
3. An Associate inputs their Exercise ID and *@revature.net* email
4. An Associate runs the tests in the skeleton and the code is sent to the RevAssess Website
    - This code to upload results is already written and already exists for Java and Python
    - The base64 encoded results are for a zip file
    - Each time the code is run It should a *different* assessment ID, that I can versions.
### Viewing Results 
1. A Trainer logs into RevAssess
2. A Trainer can look at all exercises
3. A Trainer can select any exercise and view the results

## Infrastructure
https://drive.google.com/file/d/1r2pMdDE3sTd6PwvOUuwyBRu-tnxgOwZA/view
1. An Associate sends an Assessment Payload to the Asssessment Ingestion Service
2. The Asssessment Ingestion Service verifies that the exerciseId does exist
3. The Ingestion Service publishes the payload to pub/sub
4. An Asssessment Uploader Cloud Function is triggered
5. The Function turns the base64 results back into a zip and adds it to a Cloud Storage Bucket
6. The Function saves the data to a SQL database and adds a timestamp in Unix epoch time
1. A Trainer logs in to the frontend 
2. The Authorization Service returns a JWT granting access to the REST API and is saved in local storage
    - You will have hard-coded trainers in the database
3. The Trainer is given complete access to the REST API based on their JWT
4. The REST API fetches the data from the database


#### Assessment Payload JSON 
```json
{
    "email":"BillyMcAssociate@revature.net",
    "exerciseId": 201,
    "tests":[
        {"testName":"create_user_account","isSuccessful":true, "points":10, "errorMessage":"SUCCESS"},
        {"testName":"update_user_account","isSuccessful":false, "points":20, "errorMessage":"error at line 45"},
        {"testName":"delete_user_account","isSuccessful":false, "points":15, "errorMessage":"error at line 876"},
    ],
    "sourceCode":"afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398"
}
```

## User Stories
|As a| I Want To | So That|
|----|-----------|--------|
|Trainer| Create Exercies| I can gauge my associates skill|
|Trainer| View Exercise Results| I can evaluate how the batch as a whole is doing|
|Trainer| download an individual assesment code | I can create targeted feedback|
|Associate| Submit a test| I can get evaluated|
