
// pure function
// no side effects
// The function is completely isolated from any variables NOT within the function

// given the same input you will ALWAYS get the same out put
function add(num1, num2){ // pure function
    return num1 + num2;
}

let counter = 0;

function addToCounter(num){ // impure function. 
    counter = counter + num;
    return counter;
}
// The same inputs DO NOT get you the same outputs
console.log(addToCounter(100))
console.log(addToCounter(100))
console.log(addToCounter(50))
console.log(addToCounter(50))

// Try to design code with pure functions for the most part
// Your code will be A LOT easy to debug when you do not use variables OUTSIDE of the function
// Easier to test functions that an input guarantees a specific output