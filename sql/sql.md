# Database
- In the most generic, a database is something that permanently stoes data (persistence)
    - If you turn off the power is it still there.
- Relational Databases
    - Store information in **tables**.
        - Columns are called **attributes**
        - rows are called **records**
    - These tables **relate** or connect to each other.
    - The entire structure and organization of tables and constraints for a database is called the **schema**.

# SQL (Structured Query Langugage)
- It has been the standard programming language for Databases since the 1970s
- It is a programming language
    - control flow and logic
    - Not a general purpose programming languages like JS or Java or C# Python
    - SQL is designed to do one thing and one thing only. Work with databases
        - Domain specific language (DSL)
            - CSS is another DSL. It is for styling webpages and that is it
    - Scripting langugae
        - Gives commands to a machine to run.

# Sub Languages of SQL
- DDL
    - Data Definition Language
    - Create alter or drop tables
    - Create alter or drop constraints on tables
- DML
    - Data Manipulation Language
    - CRUD operations on data *within* a table
- DQL
    - Data Query Language
    - Read in CRUD
    - Reading or *querying* information in a table
- DCL
    - Data Control Language
    - Granting permissions to users as well as creating users
    - Users are developers working on the database
- TCL
    - Transaction Control Language
    - Resposible for transaction management

# Referential Integrity
- Core concept that records can refer to records in other tables
    - A player record might have a column pointing to the team they are one.
- This reference **SHOULD ALWAYS** be valid
    - Orphan records
        - Records that point to another record that does not exist
- A **Foreign Key**
    - It is a constraint like primary key or not null or unique
    - This column MUST refer to a unqiue value in another table
    - The table that *has* the foreign key is called the **child**
    - The table that the foreign key references is called th **parent**

# RDBMS
    - Relational Database Management System
    - Software that manages your databases
    - Several popular varietes
        - MySQL
        - Postgres
        - MariaDB
        - Oracle (boo)

# Multiplicties
- There are 3 types of relationships between tables
- How many records in one table match to records in another table
    - 1-1 
        - One record in table A matches to 1 and only 1 record in table.
        - Player table could have 1-1- with a table that tracks their stats.
    - 1-many 
        - One record in table A matches to many records in table B
        - Team- Player . One team has many players. Each player has only one team.
    - many-many
        - Many records in Table A match to Many records in Table B
        - Junction tables are often used to keep track of many to many relationship
        - Player - game . One player will be part of many games. One game will have many players.

# Normalization
- The process of removing redundant information from your database.
- There are 7 normalized forms 1 least strict 7 most strict.
- More normalized DOES NOT mean better
    - Downsides
        - Your spread out in the database which can make writing queries more difficult
        - **joins** DENORMALIZE your database but stiching two tables together for a query
- 1NF (First Normalized Form)
    - Every record MUST have a unique identifier
        - Primary key
    - All data in the table must be atomic
        - The data cannot be meaningfully broken into more columns
        - The data is not array-like in nature
- 2NF (Second normalized form)
    - Should be in 1NF
    - No **functional dependencies** 
        - A column cannot be calculated with another column(s)
- 3NF (Third Normalized Form)
    - should be in 2NF
    - No **Transitive dependencies**
        - Information which could be found elsewhere in the database.

# Transactions
- Relational DBs tend to be transactional in nature
    - For the most part DATA is updated and edited in discete units **transactions**.
    - A transaction can be one to many SQL statements executed together.
- Transactions are **ACID compliant**
    - Atomic
        - A transaction works completely or everything it does is *rolled back*.
    - Consistent
        - A transaction moves the database from one consistent state to another.
        - There is not an in between state in a database.
    - Isolated
        - Transactions can run in parallel at the same time.
            - You might have multiple reads updates or inserts going on at the same time.
        - These transactions should be isolated from each other and not cause data conflicts.
            - One transaction deletes a record at the same time another transaction updates it.
    - Durable
        - Transactions fail gracefully
        - An error in a transaction does not result in data loss or data corruption.
