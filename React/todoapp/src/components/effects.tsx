import { useEffect } from "react"

export default function SideEffect(){

    // Typically in React you are writing component that are self contained
    // in a well managed lifecycle. Rendering and rerendering is nice and controlled
    // useEffect is a hook that you can use for code that has side effects
    // make changes 'outside of react'

    useEffect(()=>{
        document.getElementsByTagName("title")[0].innerHTML="To Do APP"
        // directly editing the code on the page
    })

    return(<h1>
        I edited the title of the web page
    </h1>)

}