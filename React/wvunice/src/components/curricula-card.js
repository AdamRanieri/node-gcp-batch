import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CurriculaList from './curricula-list';

export default function CurriculaCard(){

    return(<Card>
        <CardContent>
            <h3>Taught Curricula</h3>
            <CurriculaList></CurriculaList>
        </CardContent>
    </Card>)
}