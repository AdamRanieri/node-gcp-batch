// You can create object literal types on the spot for a function or variable
function highestScore(score:{intials:string, points: number}){

}

//  A highly reccomend making a class for any object types you will be reusing
// class Score{

//     intials:string;
//     points:number;

//     constructor(intials:string, points:number){
//         this.intials = intials;
//         this.points = points;
//     }
        

// }
// TS has a short syntax for creating classes
class Score{
    constructor(public initials:string = "AAA" , public points:number = 0 ){}
    displayScore(){
        console.log(this.points)
    }
}
// public: is the same as NOTHING
// private: accessible on in the current class
// protected: accessible in current and child classses



const badScore = new Score("ACR",0);

function lowestScore(score:Score){

}




