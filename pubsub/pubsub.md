# Pub Sub
- Messaging model
- It is a way for your programs and software to communicate with each other
    - Intricately linked to micoservices where independently running programs work together
![Pub Sub](https://cloud.google.com/pubsub/images/wp_flow.svg)
- Asynchronous model of sending messages
    - A message is data or any other information. JSON for instance
    - You send a message and genearlly do not get any information back
    - You should not expect information to be immediately processed.
        - It could take seconds to hours for a message to be processed

## Key Terms
- **Message**
    - Some data. Could be a JSON could be plain text.
        - in GCP all messages in pubsub get turned into base64
- **Topic**
    - A queue that hold messages.
- **Publisher** the Pub in pub sub
    - An application that will *publish* messages to a topic
- **Subscription**
    - A vested interest in the topic.
    - Another queue that reads a specific topic for messages.
    - **Polling**
        - The process by which subscriptions get messages from a topic
        - **Pull**
            - The Subscriber is responsible for checking the topic for new message
        - **Push**
            - The Topic will automatically send messages to a subsciber
- **Subscriber** the sub in pub sub
    - An application that will **consume** messages from a subscription
- **Dead Letter Queue**
    - Any message in a subscription that cannot be processed might go int a DLQ
        - No subsciber is acknowledging it 
    - Runoff topic that usually developers will take a look at to see what the problem is.
- **Ingestion**
    - Data Ingestion is the process of publishers recieving data and publishing it to topics

## Design Patterns in Pub Sub
- Ways to communicate between different services
- Fan Out
    - Duplicate a message for use by many different services
- Increaesed Resiliancy/Buffering
    - No messages are lost becuase any extra messages just build in the topic to be processed later.
![Fanout] (fanoutdesign.png)
![Resiliancy] (resiliancy.png)

