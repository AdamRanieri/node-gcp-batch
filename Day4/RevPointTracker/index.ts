import express from 'express';
import Associate from './associate';

const app = express();
app.use(express.json());

const associates: Associate[] = [];

// GET: Read
app.get("/associates", (req,res)=>{
    res.send(associates)
})


app.get("/associates/:name", (req,res)=>{
    const name:string = req.params.name;
    const filteredAssociates:Associate[] = associates.filter(a => a.name === name);
    if(filteredAssociates.length === 0){
        res.status(404);
        res.send("No associate with that name");
    }else{
        res.send(filteredAssociates[0])
    }
    
});

// POST: Create
app.post("/associates",(req,res)=>{
    const associate:Associate = req.body;
    associates.push(associate);
    console.log("HELLO")
    res.send("Added a new associate");
})

// DELETE: delete
app.delete("/associates/:name", (req,res)=>{
    const name:string = req.params.name;
    for(let i =0; i< associates.length; i++){
        if(associates[i].name === name){
            associates.splice(i,1);
            res.send("Deleted the associate");
        }
    }
})

// PATCH modify
app.patch("/associates/:name/adjustpoints/:value", (req,res)=>{
    const name = req.params.name;
    const value = req.params.value;

    for(const associate of associates){
        if(associate.name === name){
            associate.points = associate.points + Number(value);
            res.send("The new point total is " + associate.points);
        }
    }


});

// PUT: replacement
// Upsert Replace the associate if it is there. Create a new one if does not exist
app.put("/associates/:name", (req, res)=>{
    const name = req.params.name;
    const associate:Associate = req.body;

    for(let i =0 ; i < associates.length; i++){
        if(associates[i].name === name){
            associates[i] = associate;
            res.send("Updated Associate")
        }
    }
});


app.listen(3000,()=>{console.log("Server started")})