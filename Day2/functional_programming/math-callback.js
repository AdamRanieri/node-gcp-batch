const add = function(num1,num2){
    return num1 + num2;
}

const subtract = function(num1 , num2){
    return num1 - num2;
}

function performOperation(num1, num2, operation){
    const result = operation(num1,num2);
    return result;
}

function smallerNumber(a,b){
    if(a<b){
        return a;
    }else{
        return b
    }
};

console.log(performOperation(90,10,add))
console.log(performOperation(90,10,subtract))
console.log(performOperation(90,10,smallerNumber))