-- 1970's ABA basketball database
create table team(
	team_id int primary key generated always as identity,
	team_name varchar(200) not null,
	hometown varchar(200),
	mascot varchar(200)
);

select * from team;

insert into team values (default,'Grand Dunk Railroad', 'Funkytown', 'The Conductor');
insert into team values (default,'Golden State', 'San Franciso', 'Splash');
insert into team values (default,'Raptors', 'Toronto', 'The Raptor');
insert into team values (default,'The Magic', 'Orlando', 'Stuff The Magic Dragon');

delete from team where team_id = 3;


create table player(
	player_id int primary key generated always as identity,
	first_name varchar(200),
	last_name varchar(200),
	salary int,
	t_id int, -- will hold what team that they play for
	constraint fk_player_team foreign key (t_id) references team(team_id) -- foreign key
);

insert into player values (default, 'Adam', 'Ranieri', 10000000, 1);
insert into player values (default, 'Ash', 'N', 100000000, 2);
insert into player values (default, 'Anthony', 'Renwick', 210000000, 1);
insert into player values (default, 'Willis', 'Larson', 1000, 3);
insert into player values (default, 'Alejandra', 'Cajiao', 1000000000, 40); 
-- insert a player to a team that does not exit

select * from player;

create table coach(
	coach_id int primary key generated always as identity,
	first_name varchar(200),
	last_name varchar(200),
	specialty varchar(200),
	salary int,
	t_id int,
	constraint fk_coach_team foreign key (t_id) references team(team_id)
);

insert into coach values (default,'Camden','Snyder','Shooting Coach',2000000,4);
insert into coach values (default,'Jarrick','Hillery','Defense',2500000,2);
insert into coach values (default,'Gerald','Reneaue','Refreshemnt Coach',1000000,1);
insert into coach values (default,'Donovan','Merrin','Head Coach',5000000,4);

select * from coach;
select * from team;
select * from player;

-- denormalztion joins
select * from (player left join team on player.t_id = team.team_id);
select * from player;

select * from (team left join coach on team.team_id = coach.t_id); -- get every team plus matching coaches
-- it will match teams that do not have coaches to null
select * from (coach left join team on team.team_id = coach.t_id); -- get every coach plus matching teams
-- left join every record in the left table and any matches in the right table
-- right join every record in the right table and any matches in the left table
-- inner join 
-- cross join
-- These tables are generated views. They exist temporarily you are not actually changing the tables

-- what team name does a player player for
select first_name, last_name, team_name from (player left join team on player.t_id = team.team_id);


select * from (coach left join player on coach.t_id = player.t_id);

select * from (coach inner join player on coach.t_id = player.t_id); -- only matching records are returned

select *from (coach cross join player); -- a cross join matches every record to every record

-- aggregate functions
-- functions that you can call on a tables that return a SINGLE value but require multipl inputs to make sense
-- avg, max, min 

-- aggregate functions can have a group by clause where you put records into buckets based on a partictualar value
select avg(salary) from coach;
select avg(salary) from coach group by t_id;
select avg(salary), team_name from (coach left join team on coach.t_id = team_id) group by team_name;

-- set operators
-- similar to joins in that they combine two table but unlike a join there is no predicate no 'on' clause
-- vertically stack the tables on top of each other

-- helpful if you find yourself with tables that have similar fields and want to query them as a single table

select first_name,last_name,salary from player
union
select first_name,last_name,salary from coach;


-- scalar function
-- function that takes single input and generates single out
-- upper(), lower()
select reverse(first_name) from player;

create table game(
	game_id int primary key generated always as identity,
	stadium varchar(200)
);

-- junciton table connects a many to many relationship
-- a table with 2 foregin keys that each reference a seperate table

create table game_player(
	g_id int,
	p_id int,
	constraint fk_gp_game foreign key (g_id) references game(game_id),
	constraint fk_gp_player foreign key (p_id) references player(player_id)
);

-- many to many relationships are often things that can exist independently
-- there is no child-parent relationship, games and players can be created independently

insert into game values (default,'American Airlines Arena');
insert into game values (default,'O Dome');
insert into game values (default, 'Mercedes Benz');
insert into game values (default, 'FTX');
select * from game;
select * from player;
select * from game_player;

-- game_id, player_id
insert into game_player values (2,1);
insert into game_player values (2,2);
insert into game_player values (1,4);
insert into game_player values (4,4);


-- Sub query is a query within a query
select last_name,specialty,team_name from (select * from coach left join team on coach.t_id = team.team_id) as anything;

