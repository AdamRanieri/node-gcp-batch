// Destructing is just a convient syntax
// it breaks a larger structure like arrays or objects into specific variables

function findMinMax(nums = [0]){

    let min = nums[0];
    let max = nums[0];

    for(let i = 0; i< nums.length; i++){
        if(nums[i] < min){
            min = nums[i]
        }

        if(nums[i] > max){
            max = nums[i]
        }
    }
   
    return [min,max]
}

const result = findMinMax([10,50,100]);
const [smallest, largest] = findMinMax([1,5,10]);
console.log(smallest);
console.log(largest);
const x = smallest;

function createObject(){
    return {fname:"Adam", lname:"ranieri"}
}

const {fname,lname} = createObject();
console.log(fname)
console.log(lname)