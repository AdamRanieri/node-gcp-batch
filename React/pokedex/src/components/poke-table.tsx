
export default function PokeTable(props:{name:string, height:number, weight:number}){

    return(<table>
        <thead><th>name</th><th>height</th><th>weight</th></thead>
        <tbody>
            <tr><td>{props.name}</td><td>{props.height}</td>{props.weight}</tr>
        </tbody>
    </table> )
    
}