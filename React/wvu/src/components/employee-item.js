
// Many components are dynamic in that we want them to create JSX based on certain inputs
//components are functions. We want to give the function some inputs to generate a specific JSX output
// by convention the inputs are called properties, often abbreviated as props
export default function EmployeeItem(props){
    const name = props.name;
    const title = props.title;

    return(<li><b>Name:</b> {name},  <b>Title:</b> {title}</li>)
}
