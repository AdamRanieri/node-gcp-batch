import axios from 'axios';
import { useState } from 'react';
import { useRef } from 'react';
import { SyntheticEvent } from 'react';
import { Pokemon } from '../dtos/pokemon';
import PokeTable from './poke-table';


export default function PokeLookup(){

    const pokenameInput = useRef<any>(); // put any

    const [pokemon,setPokemon] = useState<Pokemon>(); // optionally set a type for your state

    async function getThatPokemon(event:SyntheticEvent){    
        const name:string =  pokenameInput.current.value
        const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`)
        const pokemon:Pokemon = response.data;
        setPokemon(pokemon)
    }

    return(<div>
        <h3>Poke Look Up</h3>

        <input placeholder="pokemon name" ref={pokenameInput}></input>
        <button onClick={getThatPokemon}>Get Pokemon</button>
        {pokemon === undefined ? <h1>No Pokemon yet</h1> : <PokeTable name={pokemon.name} height={pokemon.height} weight={pokemon.weight}></PokeTable>}
        {pokemon === undefined ? '' : <img src={pokemon.sprites.front_default}/>}

    </div>)
}