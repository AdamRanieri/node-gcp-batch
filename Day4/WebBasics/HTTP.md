# HTTP (Hyper Text Transfer Protocol)
- This protocol (messaging system) is the main way information is sent from computer to computer on the internet.
- The definition of a web server is something that can handle HTTP messages
- HTTP is a request response system.
    - Any Request will ALWAYS get a response
- A **client** will make an HTTP request
- A **web server** will reply with an HTTP Response
- Anatomy of an **HTTP Request**
    - URL https://www.espn.com/college-football/
        - Where the HTTP request is going.
    - Method (Verb)
        - What the request is *supposed* to do to the server
            - GET
                - You are requesting information from the server
            - POST
                - Sending or adding *new* information to the server
            - PUT
                - You are replacing information on the server
            - DELETE
                - You are deleting information on the server
            - PATCH
                - You are editing information on the server
    - HTTP Version
        - What verion of HTTP you are using
    - Headers
        - Key Value pairs that contain extra information about the request
            - Authorization tokens
            - Access Keys
            - Information about the data you are sending (if you are sending data)
    - Body (OPTIONAL)
        - The part of the HTTP request that contains any content you want to send.
        - Think of it like the main body of an email.
- Anatomy of an **HTTP Response**
    - HTTP Version
    - Headers
        - Key value pairs that contain extra information about the response
    - Body
        - The part of an HTTP Response that contains any conent you want to send back
    - Status Code (Describe how your request was processed)
        - 100's
            - Infomation
        - 200's
            - Successes
            - 200 OK
            - 201 Successfully Created something
        - 300's
            - Redirects
        - 400's
            - Client Error (The person making the HTTP Request)
            - 403 - Forbidden
            - 404 - Not Found
            - 405 - Unauthorized
        - 500's
            - Server Error 
# JSON (JavaScript Object Notation)
- The problem
    - The internet is made of billions of web servers.
    - All of these web servers are written in different programming languages.
        - JavaScript
        - Python
        - C#
        - Java
    - All of these web servers needed to communicate with each other and with clients.
    - Cannot expect a python object to be understood by a Java program.
        - All of this communication needs to be **Programming Language Agnostic**
- The Solution
    - We want to send data as a data type that is in every programming lanugage. ***STRING***
        - Any programming language could understand the information.
    - We wanted a format for the string that was machines to parse. 
    - The format should human readable (even if not exactly designed for humans)
    - JSON is the most common format for sending information on the web
- What is JSON
    - A JSON is just a ***STRING*** version of a JavaScript Object Literal
    - JavaScript is THE ONLY language supported in web browsers so it made sense to base a universal string format off of JavaScript.

- JSON format
    - Each key is in quotation marks.
    - All string values are in Quotation marks
    - numbers ARE NOT in quotation marks
    - booleans ARE NOT in quotation marks
    - spaces do not matter
    - Functions on an object DO NOT get turned into a JSON
        - JSON is designed to be a universal 'Lingua Franca' between web servers and clients
        - Having a function in a JSON would only be applicable to JS programs.
        - Functions do not contain information
- JSON.stringify(JSO)
    - Return a STRING JSON
- JSON.parse(jsonSTRING)
    - Return a JavaScript Object

```JavaScript
const adam = {fname:"Adam", lname:"Ranieri", age:19, isTrainer:true}
// {"fname":"Adam","lname":"Ranieri","age":19,"isTrainer":true}
```
    
    