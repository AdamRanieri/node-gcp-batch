# JavaScript
- The most popular programming language in the world
    - The only programming language in web browsers
    - Originally designed for making web pages dymanic
        - You click on something, something happens
- Brendan Eich created it for Netscape back in 1995
- JS above all else was designed to be very flexible
    - JS was designed in about 9 days
        - JS definetely has some quirks and questionable decisions in it
    - **Java**Script 100% as a marketing scheme
        - Java was the hip new language at the time and they thought naming it Java they could piggyback off the success
    - Java and JavaScript have almost nothing in common as programming languages
        - Java is to JavaScript as ham is to hamster

## Features of the language
- Dyanamic/Loosely typed language
    - Variables do are not declared with a type when you create them
```JavaScript 
    let name = "Adam" // You do not need to say this is a string
```
- Weakly typed 
    - JS will corece datatypes into one another
    - JS can and WILL turn a string into a boolean
```JavaScript
    let sum = true + true
    // JS would turn each true into 1 and add them
```
- Interpreted language
    - JS is read and executed at the same time JIT (Just In Time)
    - A line of code is read then executed
    - DIFFERENT from a language like Java or C++ where the code is compiled then executed
        - Compiled: Source code converted into machine code

- Multi-paradigmed (Cake Salad)
    - Functional Programming (In my opinion JS is a bit more functional)
        - Callbacks
        - Anonyous functions
    - Object Oriented Programming
        - Class
        - Encapsulation
        - Abstraction

- Single threaded language
    - JS can only compute/ process one thing at any given time

- Event-driven language
    - JS is designed to handle events
        - Events can be anything
        - In a web browser this could be a person clicking on a button
        - In node.js this could be handling an HTTP request

## Node.js
- JS was once 100% just for use in browsers
- In the late 2000's Ryan Dahl of Google invented node.js
- node.js is a JavaScript runtime that allows you to run JS OUTISIDE of a web browser
- Suddenly tons of application could be written in JS.
    - Analysis software
    - Web Servers
    - Desktop Applications (Electron)
- Atwood's law "Anything that can be written in JS. Will be written in JS"