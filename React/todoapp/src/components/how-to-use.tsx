

export default function HowToIntro(){

    return(<div>
        <h2>Instructions</h2>
        <ol>
            <li>Create a task in the task creation page</li>
            <li>View tasks in the task viewer page</li>
            <li>You can click on tasks that you have completed</li>
        </ol>
    </div>)
}