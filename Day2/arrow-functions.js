// ES6 created an arrow syntax for functions
// more compact syntax for writing funcitons

function hello(){
    console.log("Hello");
};
hello();

// store the function in a variable called hola
// then invoked ()
const hola = function(){
    console.log("Hola");
};
hola();

const gutentag = () => {
    console.log("Gutentag")
};
gutentag()

// function add(num1 = 0, num2 = 0){
//     return num1 + num2;
// }
const add = (num1, num2) =>{
    return num1 + num2;
};
// Arrow function syntax
// 1. if you pass in a single parameter you do not need parentheses

const greetPerson = fname =>{
    console.log("Hello" + fname)
};

greetPerson("Adam");

// 2. if your function body is ONLY a single line you do not need curly bracket
// and the result of that line is implicitly returned

function subtract(num1, num2){
    return num1 - num2;
}

const minus = (num1 =0, num2 =0) => num1 - num2 ;

const difference = minus(100,40);
console.log(difference)

const isEven = num => num%2 === 0;

console.log(isEven(901))

// DO NOT USE ARROW FUNCTIONS ON YOUR OBJECTS you are not going to get what you want
const adam = {
    fname:"Adam",
    desribeSelf: ()=>{
        // this is arrow functions does not work the same as in regular functions
        console.log("Hello my name is " + this.fname)
    }
}

adam.desribeSelf()
