const {PubSub} = require('@google-cloud/pubsub');
const pubsub = new PubSub({projectId:'reva-points-tracker-ranieri'})


async function publishMessage(){
    const suggestion = {sender:'Bob Smith', comment:'RevaPoints should have a more standardized value'}
    const response = await pubsub.topic('suggestions-topic').publishJSON(suggestion)
    console.log(response);
}

publishMessage()