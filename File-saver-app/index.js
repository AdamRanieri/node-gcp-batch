const express = require('express');
const app = express();

const {readFile, writeFile} = require('fs').promises;

// writing to a file
app.get('/addperson/:name', async (req, res) =>{
    const fileData = await readFile('./logs/people.txt');
    const info = fileData.toString();
    const updatadInfo =  info + req.params.name;
    await writeFile('./logs/people.txt',updatadInfo);
    res.send('data saved to file')
})

app.get('/people', async (req,res)=>{
    const fileData = await readFile('./logs/people.txt');
    const info = fileData.toString();
    res.send(info)
})

app.get('/reset', async (req,res)=>{
    await writeFile('./logs/people.txt','');
    res.send('reset the file data')
})

app.listen(3000, ()=>console.log('App Started'))