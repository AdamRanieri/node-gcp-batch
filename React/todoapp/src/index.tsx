import React from 'react';
import ReactDOM from 'react-dom';
import CreateTaskPage from './components/create-task-page';
import TaskViewerPage from './components/view-tasks-page';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './stores/task-store';
import SideEffect from './components/effects';

ReactDOM.render(
  <React.StrictMode>
    
    {/* <Router>

      <Route path="/create">
        <CreateTaskPage></CreateTaskPage>
      </Route>

      <Route path="/view">
        <TaskViewerPage></TaskViewerPage>
      </Route>
    
    </Router> */}
    
    <Provider store={store}> 
    {/** by wrapping these components in a provider tag they can now use 
     * the same store and share data */}
      <CreateTaskPage></CreateTaskPage>
      <TaskViewerPage></TaskViewerPage>
    </Provider>
    <SideEffect></SideEffect>

  
  </React.StrictMode>,
  document.getElementById('root')
);

