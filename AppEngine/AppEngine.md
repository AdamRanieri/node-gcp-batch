# App Engine
- The original GCP service
    - Before GCP was a giant platform for all cloud computing neeeds it offered App Engine
- To deploy an applicationis not always easy and requires a lot of configuration and maitnence
    - Create an instance
    - Install nodejs
    - Maybe create an instance template
    - Create a custom VPC 
    - Create firewall rules and network tags
    - You had to create an instance group for multiple vms
    - Create a load balancer to split traffic
- Google will take care the deployment, operations and maitnence for you
- You do lose some customizability but it will be much faster and secure

## Deploy node applications
1. app.yaml in the root directory (same place as the package.json)
2. you listen for an environment variable named PORT
3. start script in your package.json