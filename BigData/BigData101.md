# Big Data
- Data on very large scales
    - terrabyes to petabytes, zetabytes
- **Data**
    - Raw facts
    - Numbers, dates, times, strings
    - At 5:45 PM EST James Smith bought 3 shares of TSLA at $401.56/share
- **Information**
    - Meaningful insight gained from data
    - data  
        - 9/20 Ranieri Inc $100 a share
        - 9/21 Ranieri Inc $110 a share
        - 9/22 Ranieri Inc $120 a share
    - information
        - Ranieri Inc is increasing in value

## Data Structure
- Structured Data
    - Data stored in easy to analyze formats
    - It is easy to gain information from structed data is easy
    - Examples
        - Database Table
        - Excel Sheet
        - CSV
- Unstructured Data
    - Data that is NOT easy to analyze or process
    - Difficult to turn into infromation (particularly for computers)
    - Examples
        - An image
        - Audio file
        - Scanned Document
- Semi-structured Data
    - Data that has some structure but may have tough parts to analyze
    - Exmaples
        - Email
        - JSON

## Data Quality
- Consistent
    - Data that doesn't conflict with itself
- Relavent
    - Data that is up to date
- Complete
    - no missing records or fields
- Accurate
    - represents reality
- Granular
    - How specific it is

Data Quality != Data Structure
- High Quality Low Structure
    - Scanned all old newspapers
    - Video camera footage, security feeds
- High Structure Low Quality
    - Infromal online surveys

## Big Data Lifecycle
1. Ingestion
    - Getting the data
2. Store
    - House the data somewher
3. Process/analyze 
    - filter and refine the data
4. Explore and visulize
    - Make the data into Human Friendly Information
    - Humans will beed to make decisions at some point.

# GCP Services for Big Data

## BigTable
- NoSQL
    - Schemaless
- Wide Column Database
    - Opitimized for records that have A LOT of columns
- For information in the scale of petabytes
    - IoT (Internet of Things)
    - Millions of datapoints every few seconds
- **OLAP**
    - Mostly for analysis

## Dataproc
- Not a database
- Only **proc**esses data. Does not store it
- *MapReduce Processing*
    - Process information in parallel
    - Really important if you have a lot of data
- *Apache Spark* and *Hadoop*
    - Software that using MapReduce processing

    
## Cloud Datatudio
- visualizing information