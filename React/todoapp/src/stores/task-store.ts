import { createStore, Reducer } from "redux";

// We create a Task Type. Just a Helpful Intellisense thing
export interface Task{
    name:string;
    isComplete:boolean;
    priority:number;
}

// The internal application state we want to maintain is just an array of tasks
export interface TaskState{
    tasks:Task[];
}

// THE GOLDEN RULE OF WRITNG A REDUCER FUNCTION IN REDUX
// ALWAYS RETURN A TaskState. It should ALWAYS be a NEW OBJECT.
const reducer:Reducer<TaskState,any> = (taskState:TaskState = {tasks:[]}, message:any) =>{

    if(message.type === "add"){
        const newTaskState:TaskState = {tasks:[...taskState.tasks, message.task]}// duplicate an array
        return newTaskState;
    }
    
    return taskState

}

let initialState:any = JSON.parse(localStorage.getItem("taskdata")) ?? {tasks:[]};
const store = createStore(reducer, initialState); // we have created out store
store.subscribe(()=>{
    localStorage.setItem("taskdata",JSON.stringify(store.getState()))
})


export default store;