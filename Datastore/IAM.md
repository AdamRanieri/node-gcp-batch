# IAM
- Identity Access Management
- It is how GCP tracks permission to use certain resources
- IAM is for developers and resources on the cloud.
    - IT IS NOT FOR END USERS

# Terminology
- **User**
    - An actual human person who is accessing GCP
- **Role**
    - A description of a certain software position
    - This determines what permissions someone in this role can have
- **permission**
    - Ability to do something on GCP
- **Service Account**
    - A *machine* in the cloud or on premise that can be give a role
- Users or Service Accounts have Roles which grant them permissions

# Steps for making a computer use a service account
1. Create Service Account
2. Create and download the JSON key
3. Set an evironment variable GOOGLE_APPLICATION_CREDENTIALS that points to the JSON key