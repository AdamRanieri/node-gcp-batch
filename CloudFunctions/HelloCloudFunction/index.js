const {Datastore} = require('@google-cloud/datastore');
const datastore = new Datastore()

/**
 * @param {Request} req HTTP request context.
 * @param {Response} res HTTP response context.
 */
exports.hello = async (req,res)=>{
    const query = datastore.createQuery('Associate')
    const [data,metaInfo] = await datastore.runQuery(query);
    res.send(data)
}