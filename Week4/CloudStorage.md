# Cloud Storage
- A hugely important service
- Storing objects. Object Based Storage. (Files)
- Objects are stored in **buckets**
- objects can be added or removed from buckets
    - You do not edit objects in a bucket
    - You override or remove them
- Objects and buckets can be given different level of permissions and accessability
- **Static Website Hosting**
    - Frontends ends are just html css and JS files 
    - Most frontends are just these files hosted and mad public on a bucket
    - This is what Firebase was doing behind the scenes
        - Firebase also gave it a nice url
```bash

    gsutil mb gs://bucketname # make a bucket 
    gsutil copy CloudStorage.md gs://ranieri-library-bucket/cloud-store.md # upload a file to your bucket
    gsutil copy gs://ranieri-library-bucket/cloud-store.md localfilename.md

```
- **Storage Classes**
    - Standard
        - Storing objects that are frequently accessed and updated.
        - files that people commonly download.
        - or files for a public website.
    - Nearline
        - 30 days is the minimum time objects in nearline must be stored or you get charged extra
        - Monthly business reports that you might access infrequently.
    - Coldline 
        - 90 days is the minimum time objects in coldline must be stored or you get charged extra
        - Quarterly business reports. Local news company might have some unused video that goes unused but do not want to get rid of.
    - Archive
        - 365 days is the minimum time objects in archive can be store or you get charged.
        - For files that you do not anticpate needing at all or very very infrequently
        - Legally required to keep medical examination documents for 10 years.
- The cost per gigabyte of storage goes DOWN. 
- The cost of accessing each object goes UP.
