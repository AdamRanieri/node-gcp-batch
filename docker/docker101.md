# Docker
- Came out in 2013
- **Containerization Software**
- Designed to address the problem of *it works on my machine*
- Orginially designed for linux 
- Docker hosts an online image repository called **Dockerhub**
- Deploying and running applications is a constant struggle.
    - The machine has the appropirate runtime environement
    - The environment variables are correct
    - Maybe there are specific files that have certain that are hard coded
    - Every language has it's own quirks and way to start an application
        - node index.js npm start
        - py main.py
        - java -jar myapp.jar
    - You might have applications that all coded to the same port number so you cannot run them simultaneously
- Docker's primary purpose is to make deployment and operations easier.
    - It does not do anything you couldn't do manually
- Key Terminology
    - **Dockerfile**
        - A script that creates an image
        - It is a file with code inside of it
        - Dockerfile (no extension)
    - **Image**
        - A blueprint for container
        - It is *NOT* a running program
        - The schematics for a application/container
        - An image is like a Class in programming. 
    - **Container**
        - A running instance of an image
        - A container is like an object created from a class.
        - Containers are independently running isolated environments
        - You can put anything* in a container
        - Containers are lightweight
            - Not usually big resource hogs and low maitenence for the OS to handle
            - Containers are treated just like any other program for the OS.
            - From the OS perspective running a container is like a video games running in the background.
            - Containers are just another running process
- Basic Lifecycle
    1. Create a Dockerfile
    2. Dockerfile is used to create an image
    3. An image is used to create containers

```bash
# -t tag (name of the image)
docker build -t my-image-name locationofdockerfile
docker build -t bonjour-v1 . # current directory
# build will create an image

docker run image
dcocker run bonjour-v1
# create a container based off of that image
docker run -p 4444:3000 bonjour-v4
# -p publish forward any requests to localhost:4444 to port 3000 on the container 
# the first number is any number that is convienent, the second number must be the port you exposed

docker run -d -p 4444:3000 bonjour-v4
# -d detach run the container in the background. Do not attach the output to my current terminal/powershell
```
