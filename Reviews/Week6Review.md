# Week 6 Review

## Agile
- Mindset/philosophy
- There are not specific rules
- Core belief is that you should adapt, be agile in how you approach problems and quickly come up with solutions
- Agile Manifesto 4 tenets about Agile
    - **Adapt to change over following a plan**
    - **Customer Collaboration over contract**
    - **Communication over procedure**
    - **Demos over documentation**

## Waterfall
- Old School approach
- Not inherently bad
- Very restrictive and based on procedures
- *stick to the plan*
- ![Agile vs Waterfall](https://www.techguide.com.au/wp-content/uploads/20 20/01/Agile-750x430.jpeg)

## Scrum
- Implementation of Agile philosophy
- Based on iterative **sprints**
    - usually about 2 weeks in length
- Key roles
    - **Product Owner**
        - Person ultimately in charge of the success of the project
    - **Scrum Master**
        - Leads the scrum team and their job is to achieve peak efficiency
    - **Team Members**
        - The people in a scrum team working on the project
- Key Ceremonies
    - **Strory board Grooming / Sprint Planning**
        - Creating user stories and assign them
    - **Daily Standup**
        - Daily meeting to discuss what you are working on
    - **Spring Review**
        - Gather all your demos and other artificats together
    - **Sprint Retrospective**
        - Go over how the sprint went. Lessons learned. What to do for the next sprint.
![Sprint Timeline](https://www.parabol.co/hs-fs/hubfs/The%205%20Scrum%20Ceremonies%20in%20order.png?width=1518&name=The%205%20Scrum%20Ceremonies%20in%20order.png)
- **User Story**
    - Planning a feature from the perspective of end users.
    - As a *Employee* I want to *view pending orders* so that *I can follow up with the client*
    - Feature sprawl
        - Writing code for features that no one really requested


# Pub Sub
- A messaging acrhitecture
- A way for progams to communicate with one another
- An asynchronous messaging
    - Do not expect a response
    - Expect that messages might not get process immediately
- Key Terms
    - **Topic**
        - Message Queue
        - Publishers will send messages to a topic
    - **Publisher**
        - Some program/app that will send messages to a topic
    - **Subscription**
        - Vested interest in a Topic
        - For your mental model think of it as another queue that recieves messages from a topic
    - **Subscriber**
        - Program/app that will read messages from a subscription
        - Subscribers must *acknowledge* a message before it is removed from the subsrcription
    - **Polling**
        - The process by which subscriptions get messages
        - **Push**
            - The Topic will automatically send messages to the subscriptions/subscribers
        - **Pull**
            - The Subscriber is responsible for listening to the topic and reciving the messages
    - **Dead Letter Queue**
        - Messages that are unprocessable/ are never acknowledged are often shunted to a DLQ
        - This queue is usually from developers to debug what was wrong with these messages
    - **Ingestion**
        - The process by which publishers publish messages to a topic
        - *Data Ingestion*
- Pros
    - Scalable and Reliable
        - Messages can pile into the topic
        - None of the messages are lost
        - And will be processed at a later time
        - Inherent buffering effect
    - Great way to extend applications using a common interface
        - Very easy to create a new micro-service and a subsciption for that microservice
        - None of the other services or components of the architecture have to changes
- Cons
    - Asynchronous
        - Not great for application immidate feedback

![Fanout] (../pubsub/fanoutdesign.png)
![Resiliancy] (../pubsub/resiliancy.png)

## Logging
- Cloud Logging is the rebranded Stackdriver
    - Anytime you see stackdriver replace it with Cloud
- Just about everything in GCP gets logged in cloud logging
    - Creating resources
    - editing resources
    - errors
    - Any custom logs for your applications
- Logs by default get deleted after a certain time period if not specifically saved
- **Log Routers**
    - **Data Sinks**
        - In cloud logging Sinks will direct the logs to a destination
            - A storge bucket
            - BigQuery
- Severity Levels
    - Info
    - Critical
    - Error
    - Warn

## Big Query
- Data Warehouse
- It is SQL based
- And endpoint for a lot of your data.
- It is for data analysis and querying
- NOT for applications that need a database to store information 
- **OLTP**
    - Online Transaction Processing
        - Cloud SQL
        - Datastore
- **OLAP**
    - Online Analysis Processing
        - BigQuery
- BigQuery charges for two things
    - The Amount of data you store in it
    - The Amount of data that you read in your queries
    - Data streamed in
- **Dry Run**
    - GCP will estimate the total amount of data a query will generate

# Microservices
- An application is split into several independently running programs.
- These programs communicate through HTTP *usually*
![Microservice](https://res.cloudinary.com/practicaldev/image/fetch/s--seen3BGm--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://user-images.githubusercontent.com/2697570/49395813-cd094980-f737-11e8-9e9a-6c20db5720c4.jpg)
- Pros
    - A single service going down does not make the application as whole unusable.
    - Independent programs promote scalability and extensibility.
        - Very easy to add a new microservice to you software ecosystem *extensibility*.
        - Possible to scale specific services to meet demand with peak *efficiency*
    - **Polyglot** apploications are possible in microservices.
        - Application written using many languages
- Cons
    - Much harder to set up 
        - More configuration and moving parts.
    - Because services communicate via HTTP they communicate way slower than if they were in the same program.
        - Milleseconds vs Nanoseconds
    - The **Discovery Problem**
        - How do your services know the location of other services.
            - Coding an application to find a service by the IP address is usually a bad idea.
                - IP addresses in the cloud are ephemeral, constantly changing
        - Solutions
            - 1/10 excel spreadsheet with service names and IPs hosting those services
            - Registry pattern
                - Anytime a microservice instance is created. It adds itself to a registry server 
                - Any requests for a paricular service first check the registry for a proper IP address
            - Tons of 3rd party software that addresses this issue
                - Zookeeper
                - Spring Cloud
                - Consul
                - Eureka
                - Zuul (Gateway service)
            - App Engine
                - Each service has an unchanging DNS name *Magic-URls*
            - GCE allows you to set Metadata that allows you share information between instances
            - Kubenetes has its own service name resolution
![Service Discovery](https://cdn.wp.nginx.com/wp-content/uploads/2016/04/Richardson-microservices-part4-3_server-side-pattern.png)

## Infrastructure As Code (IaC)
- DevOps combines the Development of an application with the Operations of an application
- Many Cloud services are geared towards making your code and deployment meld together
    - app.yaml is a small scale example of IaC (fairly limited in its ability)
        - The file says how many instances you want
        - The runtime environment
        - The type of instance

## Terraform
- IaC tool
- Allows us to delcartively define cloud infrastructure and easily deploy and delete it.
- All of this will be in Terraform code
- IaC has a lot of advantages
    - Quickly deploy, repdeploy and delete cloud resources
    - You will not accidently make mistakes in your deploymets

```groovy

provider "google" {
    project = "reva-points-tracker-ranieri"
    region = "us-central1"
    zone = "us-central1-c"
}

resource "google_compute_instance" "reva-points-intstance" {
    name = "reva-point-server"
    machine_type = "e2-micro"

    boot_disk {
      initialize_params{
          image = "debian-cloud/debian-9"
      }
    }

    network_interface {
      network = "default"
      access_config {
        
      }

    }
}

```
# GCP ACE review
- Will be on QC and interviews
[Pub Sub](https://github.com/adamranieri/GCP-ACE/blob/main/DataServices/PubSub.md)
[Compute Engine](https://github.com/adamranieri/GCP-ACE/blob/main/Computing/ComputeEngine.md)
[Big Query](https://github.com/adamranieri/GCP-ACE/blob/main/Databases/BigQuery.md)