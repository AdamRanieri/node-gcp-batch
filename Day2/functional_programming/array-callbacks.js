// Arrays have a lot of higher order functions that take in a callback
// forEach returns void
// map returns an array
// filter returns an array

// lesser extent reduce returns a single value

const names = ["Adam", "Bill", "Steve"];

// Imperative approach
for(const name of names){
    console.log(`Hello ${name}`)
}

// functional approach
function sayHello(x){
    console.log("Hello " + x)
}
names.forEach(sayHello)// The higher order function will invoke the sayHello callback
// once for each element in the array

// in farenheit
const temps = [0, 212, 97, -20, 105];

function convertFtoC(temp){
    const c = (temp-32)*5/9;
    return c;
}

// Imperative approach

function convertToCelcius(farray){
    const celciusRay = []; 
    for(const temp of farray){
        celciusRay.push(convertFtoC(temp))
    }
    return celciusRay;
}
console.log(convertToCelcius(temps))


// functional approach
const celciusTemps = temps.map(convertFtoC) // map will call the callback function on
// each element and put the return value in a new array
console.log(celciusTemps);

function isPositive(num){ 
    return num>0;
}

const positiveTemps = temps.filter(isPositive)// filter will invoke the callback on each element
// and if it is true will put it in a new array
console.log(positiveTemps)

const temps2 = [-50,100,23,-5,3,5,100,212,300];

const positiveCelciusTemps = temps2.map(convertFtoC).filter(isPositive);
console.log(positiveCelciusTemps)

// const sum = temps2.reduce((a,b)=>a+b);
// console.log(sum)
