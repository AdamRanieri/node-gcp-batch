import TaskCreationForm from "./create-task-form";
import HowToIntro from "./how-to-use";

export default function CreateTaskPage(){

    return(<div>
        <h1>Welcome to the create task page</h1>
        <HowToIntro></HowToIntro>
        <TaskCreationForm></TaskCreationForm>
    </div>)
}