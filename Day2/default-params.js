// ES6 (Huge update for JS in 2015) added default parameters to functions

// default values for parameters if they go unused
function add(num1 = 0, num2 = 0){
    return num1 + num2;
}

const sum = add(100,50);
console.log(sum)

const sum2 = add(10);
console.log(sum2)

const sum3 = add();
console.log(sum3)

// most ides or text editors like VS code can use the default value to infer the type of the parameter
// This does not actually enforce anything but it gives you nice intellisense which is half the battle
function multiprint(phrase = '', times = 1){

    for(let i =0; i< times;i++){
        console.log(phrase)      
    }
}

multiprint("Hello",10);