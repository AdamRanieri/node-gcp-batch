
// Dummy Component. Component that has no internal state. Does not alter any data
export default function BookTable(props){

    const books = props.books;

    return(<table>
        <thead><tr><th>ID</th><th>Title</th><th>Author</th><th>Status</th></tr></thead>
        <tbody>
            {books.map(b => <tr key={b.bookId}>
                <td>{b.bookId}</td>
                <td>{b.title}</td>
                <td>{b.author}</td>
                <td>{b.isAvailable ? "Checked In" : "Checked Out"}</td>
                </tr>)}
        </tbody>
    </table>)

}