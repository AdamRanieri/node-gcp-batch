# GCP
- Google Cloud Platform
- The 3rd largest cloud provider after AWS(Amazon) and Azure(Microsoft)
- GCP was designed by developers at google not necessarily IT people so a lot of the services have nice developer friendly features.

# Cloud Computing
- Cloud computing is the abstraction of IT infrastructure to the internet.
- Computers/databases/networking routers used to have to be phyiscally bought and maintained **on premise**
    - Cons of on premise IT
        - It can be really difficult to scale or Infrastrcture
            - Order more computers and wait for them to show up
            - You might have more computers then you need.
            - Doubly true for applications and companies that deal with surges of demand. Seasonal shopping
        - It is difficult to maintain on premise infrastructure
            - Cost of housing these computers (actual physical buildings that have to be air condiditoned)
            - Staff to protect the computers and IT professionals to perform updates
- Cloud computing allows you to set up infrastructure online
    - Pros (Greatly outweigh any cons)
        - Way faster to set up.
        - Easily scale up and down the amount of resources that you are using.
        - The cloud provider will maintain the computers and protect them.
            - Less work for your IT infrastructure
    - Cons
        - Without the internet you cannot do any IT work
        - Lose some customizability. You do not have very low level access to the computers.
        - Some business do not want to be entagled to a cloud provider.

# Cloud Shell
- Every GCP has a cloud shell
- Developer terminal for working with GCP  
    - GCP creates a small linux vm with most of the GCP developer tools on it and connects you to it.
- Cloud shell does allow you run command instead of having to use the console
- Data stored on this terminal persists throughout your projects
    - Convienient way of saving information in a file and using it elsewhere

```bash
    gcloud compute instances create awesome-server
```
# gcloud SDK
- Software Development Kit
- Command line interface tool for your local machine