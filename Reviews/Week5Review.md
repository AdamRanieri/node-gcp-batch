# Week 5 Review

# IAM
- Identity Access Management
- It is how GCP tracks permission to use certain resources
- IAM is for developers and resources on the cloud.
    - IT IS NOT FOR END USERS

# Terminology
- **User**
    - An actual human person who is accessing GCP
- **Role**
    - A description of a certain software position
    - This determines what permissions someone in this role can have
- **permission**
    - Ability to do something on GCP
- **Service Account**
    - A *machine* in the cloud or on premise that can be give a role
- Users or Service Accounts have Roles which grant them permissions

# Steps for making a computer use a service account
1. Create Service Account
2. Create and download the JSON key
3. Set an evironment variable GOOGLE_APPLICATION_CREDENTIALS that points to the JSON key

## Datastore
- NoSQL database
|SQL|NoSQL|
|---|-----|
|Relational|Are not relational|
|Information stored in tables | information can be wide variety of ways|
|Postgres,SQL Server,MySQL| MongoDB, CockroachDB, *Datastore*|
|ACID Transactions| Many NoSQL do not|
|Rigid Schemas and constraints on the data| Most NoSQL do not|
- Completely managed service by GCP
    - You do not have to commission servers
    - Datastore will scale with the amount of data and requests that it recives
        - Very cost effective and elastic
- **Firestore** 
    - A newer version of Datastore
    - Has a lot of features for automatic data publishing and updates subscribed clients
        - Really nice for web and mobile applications
- Key Terminology
    - **Kind**
        - All entities in Datastore have to have a kind
        - There is no rigid defintion of property requirements unlike a table in SQL
    - **Identifier/Key**
        - Essentially the same thing as a primary key in SQL
        - Unique identifier of an entity
        - Can be auto generated
        - Supplied by the client
    - **Index**
        - Unlike a SQL database where you can make ANY Query you want
        - In datastore you can can only query properties that have been indexed
            - by default when you insert and entity the properties are indexable
        - An entity that has a lot of indexes will slow down Datastore when making insertions
        - You can choose not to index a propety if you never plan to query based on that property

## App Engine
- A computing service provided by Google
- The orginal service provided by GCP
- It is a managed service
    - GCP will be responsible for
        - deploying the code
        - autoscaling
        - loadbalancing
- Two main modes of App Engine
    - Standard
        - Directly reads your source code to deploy your application
    - Flex
        - Container based
- **app.yaml**
    - The essntial configuration file for App Engine
```yaml
runtime: nodejs14 #runtime evironment for the app
serivce: pizza-ordering-service # particular service name being deployed
service_account: datastorewriteread@joespizza.com # Service Account to use
automatic_scaling:
    min_instances: 1
    max_instances: 3
instance_class: F1 # the type of VM to deploy the application to
```
- Key Terminology
    - **Service**
        - An independently running program
            - Revapoint-transaction-service
    - **Version**
        - Everytime you deploy a service to App Engine it creates a new version
        - You can loadbalance between versions
            - Very helpful for testing out new versions before completely switching over

```bash
    gcloud app deploy 
```

## Cloud Functions
- Serverless Computing
    - Very scalable
- GCP does almost everything in terms of deploying our application
- A cloud function is a single function that can be invoked
    - you can have multiple cloud functions
- They are very popular for
    - glue code
        - stitching inputs and outputs between programs
    - Cleaning up IT infrastructure
        - Automatically checks uploads to buckets and deletes files that are the wrong data type
- Not good for
    - Anything that requires a lot of resources/processing power
        - by default they time out in 60 seconds
        - very limited memory
    - Anything that would require saving or accessing files locally
        - functions do not have any persistence
    - NEVER RECURSIVELY CALL A CLOUD FUNCTION
- Cloud functions only support a few languages
    - Go
    - Java 
    - C#
    - Ruby
    - Node
    - Python
    - PHP
- Key Terminology
    - **Event/Trigger**
        - So all functions lie dorman until a trigger causes them to activate
            - HTTP request
            - cloud pub/sub
            - Bucket uploads

## KMS Key Management Service
- You will have files that contain sensitive information and you want to make sure only certain people can read the contents
- AES
    - Advanced Encryption Standard
    - The most common encryption process in the world*
        - *kinda a guess
- **Encryption** is the process of taking data and putting into a secure format so that only authorized people can decrypt to read it
- Two main types of encryption
    - **Symmetric Encryption**
        - A single key used to *encrypt and decrypt* information
    - **Asymetric Encrption**
        - One key is used to encrypt
        - Second key is used to decrypt
```bash

gcloud kms encrypt --location=global --keyring=revapoint-keyring --key=revapoint-key --plaintext-file=dbconnection.txt --ciphertext-file=encryptedinfo.txt

gcloud kms decrypt --location=global --keyring=revapoint-keyring --key=revapoint-key --plaintext-file=decryptedfile.txt --ciphertext-file=encryptedinfo.txt
```

## GCP ACE Review Prep
- This is content that can and will be asked in 1 on 1s and QC
- [App Engine](https://github.com/adamranieri/GCP-ACE/blob/main/Computing/AppEngine.md)
- [Cloud Functions](https://github.com/adamranieri/GCP-ACE/blob/main/Computing/CloudFunctions.md)
- [Datastore](https://github.com/adamranieri/GCP-ACE/blob/main/Databases/Datastore.md)
- [IAM](https://github.com/adamranieri/GCP-ACE/blob/main/CloudPlatform/IAM.md)