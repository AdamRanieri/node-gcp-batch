import { Button, TextField } from "@material-ui/core";
import { Paper } from "@material-ui/core";
import { useRef } from "react";

export default function InformationForm(){

    const emailInput = useRef(null);

    function registerEmail(){
        const email = emailInput.current.value;
        alert("An email has been sent to " + email);
    }

    return(<Paper>
        <h4>Sign up your email for more information</h4>
        <TextField label="email address" variant="outlined" inputRef={emailInput}></TextField>
        <Button variant="contained" color="primary" onClick={registerEmail}>Register</Button>
    </Paper>)
}