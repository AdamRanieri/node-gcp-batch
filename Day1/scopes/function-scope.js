// function scope is decalred via the var keyword
// for a long time global and var were the ONLY scopes in JS
// 1. function scope leads to some weird control flow people might expect

function helloWorld(){
    var greeting = "Hello World"; // greeting was declared as var meaning it is only
    // usable within this function
    console.log(greeting)
}

// helloWorld()
// console.log(greeting) // not defined error. Cannot be used outside of the function it was declared in

function varQuirks(num){

    if(num < 100){
        var x = "Hola"; // Most program
        console.log(x)// Works
    }
    console.log(x) // Works
    // Most programming language do not let you use variables defined in a code block
    // in a function outside of that code block
}
// varQuirks(4)

// Hoisting is a "feature" of JS
// JS has a two pass system of function calls
// when you call a function
// 1. BEFORE exceuting any code any var variables in the function will be given default values of undefined
// 2. AFTER those default values are assigned the code in the function actually executes
function hoisting(){
    console.log(z);
    var z = "What's up :)";
}
hoisting();