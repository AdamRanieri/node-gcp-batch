// TypeScript has in-built support for OOP
// TS had classes BEFORE JS
// TS has access modifiers
// TS has interface
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(name, age, ssid) {
        this.name = name;
        this.age = age;
        this.ssid = ssid;
    }
    return Person;
}());
var adam = new Person("Adam", 19, 5555555555);
console.log(adam);
adam.age;
adam.name;
//adam.ssid // error for accessing private variable
var Trainer = /** @class */ (function (_super) {
    __extends(Trainer, _super);
    function Trainer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Trainer.prototype.sayHello = function () {
        console.log("Hello my name is " + this.name);
    };
    return Trainer;
}(Person));
