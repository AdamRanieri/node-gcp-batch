const {PubSub} = require('@google-cloud/pubsub');
const pubsub = new PubSub({projectId:'reva-points-tracker-ranieri'});

let counter = 0
async function pullMessages(){
    const subscription = pubsub.subscription('suggestion-analysis-subscription');

    subscription.on('message', (message) => {
        const suggestion = message.data.toString();
        console.log(suggestion)
        console.log('Got a new suggestion ' + ++counter)
        message.ack()
    })
}

pullMessages();