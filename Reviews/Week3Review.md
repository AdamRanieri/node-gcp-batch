# Client Side Techonolgy
- Your Web Browser
    - HTML/CSS renderer
    - JS Engine/Runtime
    - HTTP request mailer
- Three Pillars of Client Side
    1. HTML
        - Content and structure
    2. CSS
        - Styling
    3. JS
        - Logic/ interactive dynamic web pages

## HTML
- HyperText Markup language
- Content and structure of a web page
- HTML is made up of tag/elements
- These tags can have additional properties called **attributes**
- **Semantic Tags** are HTML elements that describe what they are
    - p, table, h1, img
    - non-semantic
        - div or span (just organization)
<h1>Hello</h1>

```html

    <h1>Hello Everyone </h1>

    <!-- self closing tag -->
    <img src="something.jpg"/>

```
- Minimal html page
```html
<!Doctype html>
<html>
    <head>

    </head>
    <body>

    </body>
</html>
```

## CSS
- Cascading Style Sheets
- Add style and aesthetics to the web page
- **Cascade Algorithm**
    - The CSS formula that determines the ultimate CSS applied when multiple selectors affect the same element
- **Selectors**
    - Ways getting elements to apply CSS
    - the tag type
        - p{some css properties}
    - the id
        - #usertable{some css properties}
    - the class
        - .someclass{some css properties}
- **Properties**
    - The key value pairs applying style
```css
    p{color:red; background-color:green}
```

## React
- JS library created by facebook
- Most popular tool for creating frontends in the world
- **DOM**
    - Docuent Object Model
    - The terminolgy of the relationship tree of elements on a web page
    ![Docuement Object Model](https://www.w3schools.com/js/pic_htmltree.gif)
- React creates a **Virtual DOM**
    - In react everything is JS.
    - React keeps track of all these JS objects and components in a virtual where it can easily detect changes in a component and surgically rerender parts of the application.
    - React then performs these edits on the REAL DOM which what people actually see.
    - React applications are essentialy one giant function that the output is the webpage
- **Single Page Application**
    - A website that is effectively a single page usually an index.html and all other pages/html is dynamically loaded in and edited and removed as the user uses the application
- **JSX**
    - JavaScript Extention
    - Expanded syntax for JS
    - React philosphy is that HTML/CSS/JS are so linked to each other on the webpage that they should meld together
    - JSX looks like HTML tags it is really JS.
- **Component**
    - A component is a reusable chunk of html/js/css
    - React has class and function based components (though I used)
```JavaScript
export default function Greeting(){

    return(<div>
        <h1>Hi Everybody</h1>
        <p>This is a small little component</p>
    </div>)
    // All components must return JSX
    // A component is always returns a single parent tag
    // Components must start with a capial letter
}
```
- **Data Flow** 
    - Data moves downard from parent elements to child elements.
    - **one -way data flow (one way data binding)**
    - **props** is the primary way data from a parent component is given to a child component

```JavaScript
export default function EmployeeList(){ // Parent element
    return(<div>
        <h3>Employee Information</h3>
        <h4>Tech Team</h4>
        <ul>
            <EmployeeItem name="Adam Ranieri" title="Lead Trainers"></EmployeeItem>
            <EmployeeItem name="Richard Orr" title="Tech Manager"></EmployeeItem>
            <EmployeeItem name="Sierra Nicholes" title="Senior Trainer"></EmployeeItem>
            {/* The attributes on a component are used to pass in our input props to the function */}
        </ul>
    </div>)
}

export default function EmployeeItem(props){ // Child Element
    const name = props.name;
    const title = props.title;

    return(<li><b>Name:</b> {name},  <b>Title:</b> {title}</li>)
}
```
## Event Handling
- React you can respond to events on the web page
- Events include a bunch of things
    - onClick
    - onMouseover
    - onKeyPress
    - etc...
- You attach and eventlistener/handler to an event on an element
- Every Event handler function recieves an event object as its first paramter when it is called

```JavaScript
export default function GreetButton(){
    function helloPopUp(event = new Event()){
        console.log(event)
        alert("Welcome !!!!!")
    }
    return(<button onClick={helloPopUp} >Click Me!</button>)
}
```
## Hooks
- All components have a lifecycle
    - created
    - rendered
    - renrenderd
    - componentDidMount
    - componentDidUpadate
- Events that occur in the existence of a component
- **Hooks** are functions that *hook* into these lifecycle events to add additional functionality to our components
```JavaScript
// some react provided hooks
const [user,setUser] = useState(); // adds statefulness to a component
const nameInput = useRef(); // consistent reference to an object in the DOM
useEffect(()=>{
    document.getElementById("somethingheading").innerHtml = "What's up";
});// directly editing the DOM/WebPage. Outside of the react virtual environment


// Redux Hooks
useSelector()
useDispatch()
```
```JavaScript
export default function Counter(){
    // [read-only value, functionToReplaceTheValue]
    const [num,setNum] = useState(0); // default value

    function addToNum(){
        const newValue = num + 1;
        setNum(newValue)
        // think of setNum as telling react to RE-RENDER the component using the new value
    }

    return(<div>
        <h3>Counter Value {num} </h3>
        <button onClick={addToNum}>Add one to num</button>
    </div>)
}
```
## Axios
- JavaScript Library for making HTTP requests
- It is one of the most popular JS libraries
- Built on top of **AJAX**
    - Asynchronous JavaScript and XML
        - Terrible name becuase people almost never use AJAX to get XML and use JSON instead
- Originally web pages had to completely reload whenever you made an HTTP request
    - Poor user experience
- AJAX allowed browsers to make HTTP requests and handle them in the JS 
    - Webpages could selectively update parts of their html
    - HTTP requests did not force the entire page to reload

## React Routing
- React is a SPA library
- You can still have routing to emulate multiple pages
- There is no in-built react router
    - You have to use some 3rd part library
- react-router is the one we used
- Within a BrowserRouter tag you can have a Swithc where you define Routes
    - A user typing in that route in the url will caluse that component to render

```JavaScript
<BrowserRouter>

    <Switch>

      <Route path="/create">
        <CreateTaskPage></CreateTaskPage>
      </Route>

      <Route path="/view">
        <TaskViewerPage></TaskViewerPage>
      </Route>

    </Switch>

</BrowserRouter>
```

## Redux
- **Flux** is a *design pattern* developed by facebook.
![Flux](https://miro.medium.com/max/570/0*rovyUqYeojiAPHrX.png)
1. The **view** (react components people interact with) generates **actions**
2. These actions are sent to a **dispatcher**
3. Dispatcher updates the **store**
4. The store passes updated information to the view
- Data flows in one direction
    - The data flow is circular but the same direction
- **Redux**
- Redux is *software* that has a similar approach to flux but slightly different
![Redux](https://miro.medium.com/max/1400/1*EdiFUfbTNmk_IxFDNqokqg.png)
1. The view genearates some sort of **action**
2. This action sends a **dispatch message** to a **reducer** function
3. This reducer function interacts with a **store** update the data within the store
4. Other components reciever information from that store 
```JavaScript
// 1. 2. listen to an action and dispatch a message
 const dispatch = useDispatch()// hook that will allow a component to dispatch messages

    function addTask(){
        dispatch({type:"add", task:task})// message to send 
    }

     return(<div>
            <button onClick={addTask}>Add Task</button>
        </div>)

//3. messages are recieved by the reducer function. Updates the store
const reducer:Reducer<TaskState,any> = (taskState:TaskState = {tasks:[]}, message:any) =>{
    if(message.type === "add"){
        const newTaskState:TaskState = {tasks:[...taskState.tasks, message.task]}// duplicate an array
        return newTaskState;
    }
    return taskState
}

//4. Updates in the state are sent out to components that have selected from the store
    const storeTasks:Task[] = useSelector((state:TaskState)=>state.tasks);// takes a callback selectorfunction
```