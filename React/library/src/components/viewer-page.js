import axios from "axios";
import { useState } from "react";
import BookTable from "./book-table";

export default function ViewerPage(){

    const [books,setBooks] = useState([])

    async function getBooks(event){
        const response = await axios.get('http://localhost:3000/books');
        setBooks(response.data)
    }

    return(<div>
        <h1>Viewer Page</h1>
        <button onClick={getBooks}>Get Books</button>
        <BookTable books={books}></BookTable>
    </div>)
}