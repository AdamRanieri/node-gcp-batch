# Kubenernetes
### Noobernetes expanded: 2 Electric Boogaloo

### The Problem
- Very large enterprises have many microservices.
    - These are always being updated
    - Always being edited
    - Want close tp 100% reliability and failsafes
    - Very exapandable/extension
        - Easy to add new services
    - Microservices could have a whole bunch of different languages and quirky runtimes

### Kubernetes as a solution
- All applications in kubernetes are based on containers
    - This means apps in the kubernetes are easy to deploy 
- Ensure reliability
    - Autoscaling
    - Loadbalancing
    - health checks to make sure your programs are always up and running
- Extensability
    - Most kubernetes is written as IaC
        - those yaml files
    - Create/recreate configurations

### Kubernetes Infrastructure
- Machines somewhere HAVE to do the processing
    - in GCP we use GKE
        - **Google Kubernetes Engine**
    - GCP just spins up VMs in GCE and connects them to a control plane to make a cluster
- **Cluster**
    - A group of VMs connected to the same **control plane**
        - clusters can have vms added or subtracted from them at anytime
- **Node**
    - A VM that is attached to a control plane
- **Kubelet**
    - This is a specific program on a node that connects it to the control plane.
    - Takes up some resources like RAM and CPU to run.
    - Gives the control plane a way to use that computer
- **Control Plane**
    - The brain of kubernetes
    - Controls all the nodes in the cluster
        - Add workloads to vms
    - **Master Node**
        - The VM that runs the control plane progam is called the master node
        - In GKE the master node is abstracted away and you do not access that vm directly
- **Node Pools**
    - Groups of vms within a cluster
    - These vms might have certain program of hardware specs that you might want to run a specific workload on.
    - There is a default node pool
        - so you always have at least one
![Kubernetes infrastructure](k8sinfrastructure.png)

### Kubnetes Objects and desired state
- You create objects for the cluster to run and maintain
- These objects created a **Desired State** of the cluster.
- The control plane's *primary responsibility* is to create and maintain this desired state
    - It will do whatever it takes 
- Objects in kubernetes
    - Pod
    - Service
        - LoadBalancer
            - Will Commission a load balancer in GCP with an external IP
        - ClusterIP
            - Creatin an internal asbstract load balancer (doesn't actually commision hardwared)
            - Purely for internal traffic
        - NodePort
            - A service that connects a specific port on a node to a pod
    - Secret
    - ServiceAccount
    - Workload objects
        - Objects that create pods for your cluster
        - Deployment
            - Pods that are stateless
            - No PVC 
        - StatefulSet
            - Pods that are statefult
            - use a PVC
        - Job
            - Create the pods, and once they are finished running delete them
            - A program that runs every once in a while to read the databases and create a businsess report. then dies.
        - Cron Jobs
            - A job that runs at a specifc time.
    - PVC
    - whole bunch more
- How these objects are defined and linked to each other is the definition of the desired state
- **Controllers**
    - The primary way the kubernetes control plane maintains and creates the desired state.
    - There is a controller responsible for every type of object
- Yaml file key terms
    - **Kind**
        - the type of object you are creating
    - **metadata**
        - High level information about the object
        - Used by controllers to identify and connect objects for the desired state
        - **name**
            - the custom name of the object you are creating
        - **Annotations**
            - Modify the Kubernetes object
            - Used to add additional functionality to the object
    - **spec**
        - technial specifictions of the object you are creating
```yaml
apiVersion: apps/v1
kind: Deployment # what type of kubernetes resource/object we are creating
metadata: # information about the kubernetes resource. It's name in kubernetes for instance
  name: hello-world-deployment
spec: # Technial blueprint for the object we are creating
  replicas: 3 # deployement specs. How much are we making
  selector:
    matchLabels: 
      app: greeting 
  template: # template for a pod
    metadata:
      labels:
        app: greeting
    spec: # spec of the pod
      containers:
        - name: hello-container
          image: adaman94/hello-app
          ports:
            - containerPort: 3000  
```
![Desired State](desiredstate.png)

### Anatomy of a pod
- Kubernetes *DOES NOT* directly run containers.
- Containers run in **pods**
    - These are self contained running processes in kubernete
    - They consist of 1-many containers running inside of a pod
        - Most pods run only a single container
![Pod Anatomy](podsandservices.png)

### Ingress
- Having an external load balancer for every service is not ideal for a couple reasons.
    - Every service would have have different IP address to reach the service
    - Some services you might not want accessible outisde the cluster
    - Load Balancers are pretty pricey
- An ingress will allow you to create a SINGLE External Load Balancer. And then create rules that will direct traffic from that load balancer to specific services
    - Ingresses allow us to route base on the URI
        - A single IP address and the services you have nice uri /myservice prefixes for
        - A backup route if a request does not match anything.
    - Check for headers
    - Route based metrics 
- Kubernetes does not come with a ingress object
    - It is as if kubernetes comes with an abstract class of an ingress but not an actual implementation.
- nginx was the ingress we downloaded and put into our cluster
![Ingress](https://i.imgur.com/rQrwNSl.png)

### Helm
- Helm a package managment tool for Kubernetes
    - GKE Helm comes already installed
- This is how we downloaded the nginx ingress
- *npm is to node as helm is to kubernetes*
- Helm does not use the term package, it uses the term **Chart**
- Downloading a chart is like installing a dependency in node.js

### Monitoring and logging in GKE
- Monitoring and logging is pretty easy
    - When you create the cluster make sure logging and monitoring is enable
- Any print statement from a container is automatically logged and sent to cloud logging

### Accessing GCP services in GKE
- The VMs/nodes can be given service accounts.
    - Not preferred if possible.
    - Overgranting of permissions often occurs.
- Attach a read-only volume containg the JSON key for a service account to the pod
    - Not fun to set up
    - GCP reccomends against creating and using JSON keys whenever possible.
        - Keys can lost or forgotten about
        - A security hazard.
- **Workload Identity**
    - Create Kubernetes Service account and link it to a GCP service account
    - Gives a specific pod the identity of that service account.
    - Not strictly for K8s.
        - Can be used for machines/software not on the GCP cloud
        - VMs on AWS that you wanted GCP to recognize as having a specific service account.

### Micro-services and discovery on Kubernetes
- How does Kubernetes solve the discovery problem?
    - Kubernetes has it's own service name resolution
    - Your services can reference each other via the service name as opposed to an IP address within the cluster.
        - Very similar the App Engine appraoch where each service has a fixed URL that will never change.






