
export default function CurriculaList(){

    const curricula = ["Java Microservices","Java Automation",
    "Node Cloud Developer","Python Automation","React Native"]
    const curriculaItems = curricula.map(i => <li>{i}</li>); 
    // creating an arra of JSX elements based off of an array of strings


    return(<div>

        <h3>Curricula</h3>
        <ul>
            {curriculaItems}
        </ul>
        </div>)
}