# Linux
- Linux is an opearting system like iOS  or Windows
- Most linux distrobutions (distros) lack a graphical user interface
    - EVERYTHING is done via text/terminal
- Why is linux so popular for web servers and hosting web applications?
    - Free (Biggest plus)
    - Very lean for a computer to run. (Windows will take up more processing and memory)
    - Very customizable
- History of linux
    - Unix was the first really influential operating system
    - It was owned by AT&T and was very expensive for companies to use.
    - Linus Torvalds wrote a version of Unix that was open source (Linux)
        - He also developed git

```bash
# important linux commands
ls # list everything in the directory
pwd # print working directory
cd # Change Directory
sudo # super user do

apt # package manager for debian based linux. NPM is just for node. apt is for downloading software for linux machines
touch # create a file

vim # text editor on linux machine. VSCODE except with no features
cat # print out contents
rm # remove
clear # clear screen
& # can be used to run a program in the background
curl # make an HTTP request (defaults to get)

```

```bash

sudo apt install nodejs # install nodejs on our computer
sudo apt install nodejs npm # insall npm


```