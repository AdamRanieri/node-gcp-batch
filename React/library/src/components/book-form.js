import axios from "axios";
import { useRef } from "react"

export default function BookForm(){

    const titleInput = useRef(null);
    const authorInput = useRef(null);
    const qualityInput = useRef(null);

    async function addBook(){

        const book = {
            bookId:0,
            title:titleInput.current.value,
            author:authorInput.current.value,
            quality:Number(qualityInput.current.value),
            isAvailable:true,
            returnDate:0
        }

        const response = await axios.post('http://localhost:3000/books',book)
        alert("You created a new book :)")

    }


    return(<div>

        <h3>Book Form</h3>

        <input placeholder="title" ref={titleInput}></input>
        <input placeholder="author" ref={authorInput}></input>
        <input placeholder="quality" ref={qualityInput} type="number"></input>
        <button onClick={addBook}>Add Book</button>

    </div>)
}